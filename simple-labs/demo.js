// Check if Web Serial API is supported
if ('serial' in navigator) {
    console.log('Web Serial API 是可用的。');
   } else {
    console.log('Web Serial API 不可用。');
   }
    
   // open the serial port
   function openSerialPort() {
    //navigator.serial.requestPort({ filters: [{ vendorId: '1a86' }] })
    navigator.serial.requestPort()
      .then(function(port) {
        return port.open({ baudRate: 115200 });
      })
      .then(function(port) {
        console.log('串行端口已打开:', port);
        port.on('data', function(data) {
          var output = document.getElementById('output');
          output.textContent += data + '\n';
        });
      })
      .catch(function(error) {
        console.error('打开串行端口失败:', error);
      });
   }
    
   // close serial port
   function closeSerialPort() {
    if (navigator.serial.port) {
      navigator.serial.port.close();
      console.log('串行端口已关闭。');
    }
   }
    
   // bond event handler to the button
   document.getElementById('openPort').addEventListener('click', openSerialPort);
   document.getElementById('closePort').addEventListener('click', closeSerialPort);